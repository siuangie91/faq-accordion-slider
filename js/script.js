//accordion
var action = "click";
var speed = 500;

$(document).ready(function() {
   $('li.q').on(action, function() {
        $(this).next().slideToggle(speed)
            //close all other answers
            .siblings('li.a').slideUp(speed);   
        
        //get img for active question
        img = $(this).find('img');
        //remove 'rotate' class for all imgs except the active
        $('img').not(img).removeClass('rotate');
        img.toggleClass('rotate');
   });
});