# jQuery Accordion Slider #

Does ***not*** use jQuery UI. From Eduonix's [**Projects In JavaScript & JQuery course**](https://stackskills.com/courses/projects-in-javascript-jquery) on **Stackskills**.

![screencapture-file-Users-AngieSiuPC-Desktop-Tutorials-Coding-Misc-_Projects-JS-20Practice-FAQ-20Accordion-20Slider-index-html-1451792876923.png](https://bitbucket.org/repo/K4bjrk/images/3410228082-screencapture-file-Users-AngieSiuPC-Desktop-Tutorials-Coding-Misc-_Projects-JS-20Practice-FAQ-20Accordion-20Slider-index-html-1451792876923.png)